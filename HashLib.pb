﻿CompilerIf #PB_Compiler_Unicode
  Macro PeekUB(a)
    PeekC(a) & $FF
  EndMacro
CompilerElse
  Macro PeekUB(a)
    PeekC(a)
  EndMacro
CompilerEndIf

Import "msvcrt.lib"
EndImport

Import "HashLib.lib"
  HAVAL(a.i, b.i, c.i) As "_HAVAL@12"
  haval_end(a.i, b.i) As "_haval_end"
  haval_hash(a.i, b.i, c.i) As "_haval_hash"
  haval_start(a.i) As "_haval_start"
  GOST(a.i, b.i, c.i) As "_GOST@12"
  GOST_Init() As "_GOST_Init@0"
  GOST_Reset(a.i) As "_GOST_Reset@4"
  GOST_Update(a.i, b.i, c.i) As "_GOST_Update@12"
  GOST_Final(a.i, b.i) As "_GOST_Final@8"
  SHA(a.i, b.i, c.i) As "_SHA@12"
  SHA_Final(a.i, b.i) As "_SHA_Final@8"
  SHA_Init(a.i) As "_SHA_Init@4"
  SHA_Update(a.i, b.i, c.i) As "_SHA_Update@12"
  SHA384(a.i, b.i, c.i) As "_SHA384@12"
  SHA384_Final(a.i, b.i) As "_SHA384_Final@8"
  SHA384_Init(a.i) As "_SHA384_Init@4"
  SHA384_Update(a.i, b.i, c.i) As "_SHA384_Update@12"
  SHA512(a.i, b.i, c.i) As "_SHA512@12"
  SHA512_Final(a.i, b.i) As "_SHA512_Final@8"
  SHA512_Init(a.i) As "_SHA512_Init@4"
  SHA512_Update(a.i, b.i, c.i) As "_SHA512_Update@12"
  SHA224(a.i, b.i, c.i) As "_SHA224@12"
  SHA224_Final(a.i, b.i) As "_SHA224_Final@8"
  SHA224_Init(a.i) As "_SHA224_Init@4"
  SHA224_Update(a.i, b.i, c.i) As "_SHA224_Update@12"
  SHA256(a.i, b.i, c.i) As "_SHA256@12"
  SHA256_Final(a.i, b.i) As "_SHA256_Final@8"
  SHA256_Init(a.i) As "_SHA256_Init@4"
  SHA256_Update(a.i, b.i, c.i) As "_SHA256_Update@12"
  SHA1(a.i, b.i, c.i) As "_SHA1@12"
  SHA1_Init(a.i) As "_SHA1_Init@4"
  SHA1_Update(a.i, b.i, c.i) As "_SHA1_Update@12"
  SHA1_Final(a.i, b.i) As "_SHA1_Final@8"
  RIPEMD160(a.i, b.i, c.i) As "_RIPEMD160@12"
  RIPEMD160_Init(a.i) As "_RIPEMD160_Init@4"
  RIPEMD160_Update(a.i, b.i, c.i) As "_RIPEMD160_Update@12"
  RIPEMD160_Final(a.i, b.i) As "_RIPEMD160_Final@8"
  MD5(a.i, b.i, c.i) As "_MD5@12"
  MD5_Final(a.i, b.i) As "_MD5_Final@8"
  MD5_Init(a.i) As "_MD5_Init@4"
  MD5_Update(a.i, b.i, c.i) As "_MD5_Update@12"
  MD4(a.i, b.i, c.i) As "_MD4@12"
  MD4_Final(a.i, b.i) As "_MD4_Final@8"
  MD4_Init(a.i) As "_MD4_Init@4"
  MD4_Update(a.i, b.i, c.i) As "_MD4_Update@12"
  MD2(a.i, b.i, c.i) As "_MD2@12"
  MD2_Final(a.i, b.i) As "_MD2_Final@8"
  MD2_Init(a.i) As "_MD2_Init@4"
  MD2_Update(a.i, b.i, c.i) As "_MD2_Update@12"
  Whirlpool(a.i, b.i, c.i) As "_Whirlpool@12"
  Whirlpool_Final(a.i, b.i) As "_Whirlpool_Final@8"
  Whirlpool_Init(a.i) As "_Whirlpool_Init@4"
  Whirlpool_Update(a.i, b.i, c.i) As "_Whirlpool_Update@12"
EndImport

;- Whirlpool Hash

ProcedureDLL.s PureHash_WhirlpoolFingerprint(*Buffer, length)
  Protected BufferOut, x, strOut.s
  
  BufferOut = AllocateMemory(64)
  
  For x = 0 To 64-1
    PokeC(BufferOut + x, 0)
  Next
  
  Whirlpool(*Buffer, length, BufferOut)
  
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

Structure NESSIEstruct
  bitLength.i[32]
  Buffer.i[64]
  bufferBits.i
  bufferPos.i
  hash.i[64/8]
EndStructure

ProcedureDLL.s PureHash_WhirlpoolFileFingerprint(filename.s)
  Protected BufferOut, x, strOut.s
  Protected BufferIn, Len = 1
  Protected c.NESSIEstruct
  
  BufferIn = AllocateMemory(1024)
  BufferOut = AllocateMemory(64)
  
  If ReadFile(0, filename)
    Whirlpool_Init(@c)
    While (Len)
      Len = ReadData(0, BufferIn, 1024)
      Whirlpool_Update(@c, BufferIn, Len)
    Wend
    Whirlpool_Final(BufferOut, @c)
    CloseFile(0)
    
    For x = 0 To 64-1
      strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
    Next
  EndIf
  
  FreeMemory(BufferIn)
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

;- RIPEMD160

ProcedureDLL.s PureHash_RIPEMD160Fingerprint(*Buffer, length)
  Protected BufferOut, x, strOut.s
  
  BufferOut = AllocateMemory(20)
  RIPEMD160(*Buffer, length, BufferOut)
  
  For x = 0 To 20-1
    strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
  Next
  
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

Structure RIPEMD160_CTX
  a.i
  b.i
  c.i
  D.i
  E.i
  Nl.i
  Nh.i
  Datas.i[64/4]
  num.i
EndStructure

ProcedureDLL.s PureHash_RIPEMD160FileFingerprint(filename.s)
  Protected BufferOut, x, strOut.s
  Protected BufferIn, Len = 1
  Protected c.RIPEMD160_CTX
  
  BufferIn = AllocateMemory(1024)
  BufferOut = AllocateMemory(20)
  
  If ReadFile(0, filename)
    RIPEMD160_Init(@c)
    While (Len)
      Len = ReadData(0, BufferIn, 1024)
      RIPEMD160_Update(@c, BufferIn, Len)
    Wend
    RIPEMD160_Final(BufferOut, @c)
    CloseFile(0)
    
    For x = 0 To 20-1
      strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
    Next
  EndIf
  
  FreeMemory(BufferIn)
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

;- MD2

ProcedureDLL.s PureHash_MD2Fingerprint(*Buffer, length)
  Protected BufferOut, x, strOut.s
  
  BufferOut = AllocateMemory(16)
  MD2(*Buffer, length, BufferOut)
  
  For x = 0 To 16-1
    strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
  Next
  
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

Structure MD2_CTX
  num.i
  Datas.i[16]
  cksm.i[16]
  state.i[16]
EndStructure

ProcedureDLL.s PureHash_MD2FileFingerprint(filename.s)
  Protected BufferOut, x, strOut.s
  Protected BufferIn, Len = 1
  Protected c.MD2_CTX
  
  BufferIn = AllocateMemory(1024)
  BufferOut = AllocateMemory(16)
  
  If ReadFile(0, filename)
    MD2_Init(@c)
    While (Len)
      Len = ReadData(0, BufferIn, 1024)
      MD2_Update(@c, BufferIn, Len)
    Wend
    MD2_Final(BufferOut, @c)
    CloseFile(0)
    
    For x = 0 To 16-1
      strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
    Next
  EndIf
  
  FreeMemory(BufferIn)
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

;- MD4

ProcedureDLL.s PureHash_MD4Fingerprint(*Buffer, length)
  Protected BufferOut, x, strOut.s
  
  BufferOut = AllocateMemory(16)
  MD4(*Buffer, length, BufferOut)
  
  For x = 0 To 16-1
    strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
  Next
  
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

Structure MD4_CTX
  a.i
  b.i
  c.i
  D.i
  E.i
  Nl.i
  Nh.i
  Datas.i[64/4]
  num.i
EndStructure

ProcedureDLL.s PureHash_MD4FileFingerprint(filename.s)
  Protected BufferOut, x, strOut.s
  Protected BufferIn, Len = 1
  Protected c.MD4_CTX
  
  BufferIn = AllocateMemory(1024)
  BufferOut = AllocateMemory(16)
  
  If ReadFile(0, filename)
    MD4_Init(@c)
    While (Len)
      Len = ReadData(0, BufferIn, 1024)
      MD4_Update(@c, BufferIn, Len)
    Wend
    MD4_Final(BufferOut, @c)
    CloseFile(0)
    
    For x = 0 To 16-1
      strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
    Next
  EndIf
  
  FreeMemory(BufferIn)
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

;- MD5

ProcedureDLL.s PureHash_MD5Fingerprint(*Buffer, length)
  Protected BufferOut, x, strOut.s
  
  BufferOut = AllocateMemory(16)
  MD5(*Buffer, length, BufferOut)
  
  For x = 0 To 16-1
    strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
  Next
  
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

Structure MD5_CTX
  a.i
  b.i
  c.i
  D.i
  E.i
  Nl.i
  Nh.i
  Datas.i[64/4]
  num.i
EndStructure

ProcedureDLL.s PureHash_MD5FileFingerprint(filename.s)
  Protected BufferOut, x, strOut.s
  Protected BufferIn, Len = 1
  Protected c.MD5_CTX
  
  BufferIn = AllocateMemory(1024)
  BufferOut = AllocateMemory(16)
  If ReadFile(0, filename)
    MD5_Init(@c)
    While (Len)
      Len = ReadData(0, BufferIn, 1024)
      MD5_Update(@c, BufferIn, Len)
    Wend
    MD5_Final(BufferOut, @c)
    CloseFile(0)
    
    For x = 0 To 16-1
      strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
    Next
  EndIf
  
  FreeMemory(BufferIn)
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

;- HAVAL

ProcedureDLL.s PureHash_HAVAL256Fingerprint(*Buffer, length)
  Protected BufferOut, x, strOut.s
  
  BufferOut = AllocateMemory(32)
  HAVAL(*Buffer, length, BufferOut)
  
  For x = 0 To 32-1
    strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
  Next
  
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

Structure haval_state
  count.i[2]
  fingerprint.i[8]
  block.i[32]
  remainder.c[32*4]
EndStructure

; ProcedureDLL.s PureHash_HAVAL256FileFingerprint(filename.s)
; Protected BufferOut, x, strOut.s
; Protected BufferIn, Len
; Protected c.haval_state
; _validate
; BufferIn = AllocateMemory(1024)
; BufferOut = AllocateMemory(32)
; _validate
; If ReadFile(0, filename)
; haval_start(@c)
; _validate
; While (Len = ReadData(0, BufferIn, 1024))
; haval_hash(@c, BufferIn, Len)
; _validate
; Wend
; haval_end (@c, BufferOut)
; _validate
; CloseFile(0)
;
; For x = 0 To 32-1
; strOut + RSet(Hex(PeekUB(BufferOut+x)), 2, "0")
; Next
; EndIf
; _validate
; FreeMemory(BufferIn)
; FreeMemory(BufferOut)
;
; ProcedureReturn strOut
; EndProcedure

;- GOST

ProcedureDLL.s PureHash_GOSTFingerprint(*Buffer, length)
  Protected BufferOut, x, strOut.s
  
  BufferOut = AllocateMemory(32)
  GOST(*Buffer, length, BufferOut)
  
  For x = 0 To 32-1
    strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
  Next
  
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

Structure GostHashCtx
  sum.i[8]
  hash.i[8]
  Len.i[8]
  partial.c[32]
  partial_bytes.i
EndStructure

ProcedureDLL.s PureHash_GOSTFileFingerprint(filename.s)
  Protected BufferOut, x, strOut.s
  Protected BufferIn, Len = 1
  Protected c.GostHashCtx
  
  BufferIn = AllocateMemory(1024)
  BufferOut = AllocateMemory(32)
  
  If ReadFile(0, filename)
    GOST_Init()
    GOST_Reset(@c)
    While (Len)
      Len = ReadData(0, BufferIn, 1024)
      GOST_Update(@c, BufferIn, Len)
    Wend
    GOST_Final(@c, BufferOut)
    CloseFile(0)
    
    For x = 0 To 32-1
      strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
    Next
  EndIf
  
  FreeMemory(BufferIn)
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

;- SHA

ProcedureDLL.s PureHash_SHAFingerprint(*Buffer, length)
  Protected BufferOut, x, strOut.s
  
  BufferOut = AllocateMemory(20)
  SHA(*Buffer, length, BufferOut)
  
  For x = 0 To 20-1
    strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
  Next
  
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

Structure SHA_CTX
	h0.i
  h1.i
  h2.i
  h3.i
  h4.i
	Nl.i
  Nh.i
	Data.i[16]
	num.i
EndStructure

ProcedureDLL.s PureHash_SHAFileFingerprint(filename.s)
  Protected BufferOut, x, strOut.s
  Protected BufferIn, Len = 1
  Protected c.SHA_CTX
  
  BufferIn = AllocateMemory(1024)
  BufferOut = AllocateMemory(20)
  
  If ReadFile(0, filename)
    SHA_Init(@c)
    While (Len)
      Len = ReadData(0, BufferIn, 1024)
      SHA_Update(@c, BufferIn, Len)
    Wend
    SHA_Final(BufferOut, @c)
    CloseFile(0)
    For x = 0 To 20-1
      strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
    Next
  EndIf
  
  FreeMemory(BufferIn)
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

;- SHA1

ProcedureDLL.s PureHash_SHA1Fingerprint(*Buffer, length)
  Protected BufferOut, x, strOut.s
  
  BufferOut = AllocateMemory(20)
  SHA1(*Buffer, length, BufferOut)
  
  For x = 0 To 20-1
    strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
  Next
  
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

ProcedureDLL.s PureHash_SHA1FileFingerprint(filename.s)
  Protected BufferOut, x, strOut.s
  Protected BufferIn, Len = 1
  Protected c.SHA_CTX
  
  BufferIn = AllocateMemory(1024)
  BufferOut = AllocateMemory(20)
  
  If ReadFile(0, filename)
    SHA1_Init(@c)
    While (Len)
      Len = ReadData(0, BufferIn, 1024)
      SHA1_Update(@c, BufferIn, Len)
    Wend
    SHA1_Final(BufferOut, @c)
    CloseFile(0)
    
    For x = 0 To 20-1
      strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
    Next
  EndIf
  
  FreeMemory(BufferIn)
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

;- SHA224

ProcedureDLL.s PureHash_SHA224Fingerprint(*Buffer, length)
  Protected BufferOut, x, strOut.s
  
  BufferOut = AllocateMemory(28)
  SHA224(*Buffer, length, BufferOut)
  
  For x = 0 To 28-1
    strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
  Next
  
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

Structure SHA256_CTX
  h.q[8]
	Nl.i
  Nh.i
	Data.i[16]
	num.i
  md_len.i
EndStructure

Structure SHA512_CTX
  h.q[8]
  Nl.q
  Nh.q
  StructureUnion
    D.q[16]
    p.c[16*8]
  EndStructureUnion
  num.i
  md_len.i
EndStructure

ProcedureDLL.s PureHash_SHA224FileFingerprint(filename.s)
  Protected BufferOut, x, strOut.s
  Protected BufferIn, Len = 1
  Protected c.SHA256_CTX
  
  BufferIn = AllocateMemory(1024)
  BufferOut = AllocateMemory(28)
  
  If ReadFile(0, filename)
    SHA224_Init(@c)
    While (Len)
      Len = ReadData(0, BufferIn, 1024)
      SHA224_Update(@c, BufferIn, Len)
    Wend
    SHA224_Final(BufferOut, @c)
    CloseFile(0)
    
    For x = 0 To 28-1
      strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
    Next
  EndIf
  
  FreeMemory(BufferIn)
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

;- SHA256

ProcedureDLL.s PureHash_SHA256Fingerprint(*Buffer, length)
  Protected BufferOut, x, strOut.s
  
  BufferOut = AllocateMemory(32)
  SHA256(*Buffer, length, BufferOut)
  
  For x = 0 To 32-1
    strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
  Next
  
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

ProcedureDLL.s PureHash_SHA256FileFingerprint(filename.s)
  Protected BufferOut, x, strOut.s
  Protected BufferIn, Len = 1
  Protected c.SHA256_CTX
  
  BufferIn = AllocateMemory(1024)
  BufferOut = AllocateMemory(32)
  
  If ReadFile(0, filename)
    SHA256_Init(@c)
    While (Len)
      Len = ReadData(0, BufferIn, 1024)
      SHA256_Update(@c, BufferIn, Len)
    Wend
    SHA256_Final(BufferOut, @c)
    CloseFile(0)
    
    For x = 0 To 32-1
      strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
    Next
  EndIf
  
  FreeMemory(BufferIn)
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

;- SHA384

ProcedureDLL.s PureHash_SHA384Fingerprint(*Buffer, length)
  Protected BufferOut, x, strOut.s
  
  BufferOut = AllocateMemory(48)
  SHA384(*Buffer, length, BufferOut)
  
  For x = 0 To 48-1
    strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
  Next
  
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

ProcedureDLL.s PureHash_SHA384FileFingerprint(filename.s)
  Protected BufferOut, x, strOut.s
  Protected BufferIn, Len = 1
  Protected c.SHA512_CTX
  
  BufferIn = AllocateMemory(1024)
  BufferOut = AllocateMemory(48)
  
  If ReadFile(0, filename)
    SHA384_Init(@c)
    While (Len)
      Len = ReadData(0, BufferIn, 1024)
      SHA384_Update(@c, BufferIn, Len)
    Wend
    SHA384_Final(BufferOut, @c)
    CloseFile(0)
    
    For x = 0 To 48-1
      strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
    Next
  EndIf
  
  FreeMemory(BufferIn)
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

;- SHA512

ProcedureDLL.s PureHash_SHA512Fingerprint(*Buffer, length)
  Protected BufferOut, x, strOut.s
  
  BufferOut = AllocateMemory(64)
  SHA512(*Buffer, length, BufferOut)
  
  For x = 0 To 64-1
    strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
  Next
  
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure

ProcedureDLL.s PureHash_SHA512FileFingerprint(filename.s)
  Protected BufferOut, x, strOut.s
  Protected BufferIn, Len = 1
  Protected c.SHA512_CTX
  
  BufferIn = AllocateMemory(1024)
  BufferOut = AllocateMemory(64)
  
  If ReadFile(0, filename)
    SHA512_Init(@c)
    While (Len)
      Len = ReadData(0, BufferIn, 1024)
      SHA512_Update(@c, BufferIn, Len)
    Wend
    SHA512_Final(BufferOut, @c)
    CloseFile(0)
    
    For x = 0 To 64-1
      strOut + RSet(Hex(PeekUB(BufferOut + x)), 2, "0")
    Next
  EndIf
  
  FreeMemory(BufferIn)
  FreeMemory(BufferOut)
  
  ProcedureReturn strOut
EndProcedure
; IDE Options = PureBasic 5.62 (Windows - x64)
; Folding = -----
; EnableAsm
; EnableThread
; Compiler = PureBasic 5.62 (Windows - x86)
; EnablePurifier
; EnableUnicode